#include "curve.h"
#include "ui_curve.h"
#include <qwt_plot_curve.h>
#include <qwt_plot_canvas.h>
#include <qwt_plot_item.h>
#include <iostream>

Curve::Curve(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Curve)
{
    ui->setupUi(this);

    ui->qwtCurve->setAxisScale(QwtPlot::xBottom,0,255);

    ui->qwtCurve->setAxisScale(QwtPlot::yLeft,0,255);

    QBrush brush = ui->qwtCurve->canvasBackground();

    brush.setColor(Qt::white);

    ui->qwtCurve->setCanvasBackground(brush);

    picker = new QwtPlotPicker(ui->qwtCurve);

}

Curve::~Curve()
{
    delete ui;
}

void Curve::getPhotoData(QImage ImageData){

    temp = ImageData;

    for (int i = 0;i< 256;i++)

        redTable[i] = blueTable[i] = greenTable[i] = 0;

    for (int i = 0;i< temp.width();i++){

        for (int j = 0;j < temp.height();j++){

            QColor clrCurrent( temp.pixel(i,j) );

            redTable[clrCurrent.red()]++;

            blueTable[clrCurrent.blue()]++;

            greenTable[clrCurrent.green()]++;

        }

    }

    for (int i = 0;i< 256;i++){

        tempRedTable[i] = (double)redTable[i]/((double)temp.height()*(double)temp.width());

        tempBlueTable[i] = (double)blueTable[i]/((double)temp.height()*(double)temp.width());

        tempGreenTable[i] = (double)greenTable[i]/((double)temp.height()*(double)temp.width());

    }

    QwtPlotCurve *red = new QwtPlotCurve();


    red->setBrush(QColor(255,0,0,125));

    red->setStyle( QwtPlotCurve::  Dots );

    QwtPlotCurve *blue = new QwtPlotCurve();

    blue->setBrush(QColor(0,0,255,125));

    blue->setStyle( QwtPlotCurve:: Dots );

    QwtPlotCurve *green = new QwtPlotCurve();

    green->setBrush(QColor(0,255,0,125));

    green->setStyle( QwtPlotCurve::  Dots );

    QVector<QPointF> redPoint;

    for(int i =0;i<256;i++){

        QPointF point;

        point.setX(i);

        point.setY(tempRedTable[i]*256*50);

        redPoint.append(point);

    }

    QVector<QPointF> greenPoint;

    for(int i =0;i<256;i++){

        QPointF point;

        point.setX(i);

        point.setY(tempGreenTable[i]*256*50);

        greenPoint.append(point);

    }

    QVector<QPointF> bluePoint;

    for(int i =0;i<256;i++){

        QPointF point;

        point.setX(i);

        point.setY(tempBlueTable[i]*256*50);

        bluePoint.append(point);

    }

    QVector<QPointF> linePoint;

    for(int i =0;i<256;i++){

        QPointF point;

        point.setX(i);

        point.setY(i);

        linePoint.append(point);

    }


    QwtPointSeriesData* redSeries = new QwtPointSeriesData(redPoint);

    QwtPointSeriesData* blueSeries = new QwtPointSeriesData(bluePoint);

    QwtPointSeriesData* greenSeries = new QwtPointSeriesData(greenPoint);

    QwtPointSeriesData* lineSeries = new QwtPointSeriesData(linePoint);

    red->setSamples(redSeries);

    red->attach(picker->plot());


    green->setSamples(greenSeries);

    green->attach(picker->plot());

    blue->setSamples(blueSeries);

    blue->attach(picker->plot());

    function = new QwtPlotCurve ();

    function ->setPen(QPen(Qt::black,3));

    function->setLegendAttribute(QwtPlotCurve::LegendShowLine,true);

    function->setSamples(lineSeries);

    function->attach(picker->plot());

    ui->qwtCurve->replot();

    ui->qwtCurve->show();

    if (picker->MouseSelect1){

        int tempX = picker->xAxis();

        int tempY = picker->yAxis();

        std::cout <<  tempX << ' ' << tempY<<'\n';

    }

}
