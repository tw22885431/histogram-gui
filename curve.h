#ifndef CURVE_H
#define CURVE_H

#include <QDialog>
#include <qwt_plot_curve.h>
#include <qwt_plot_picker.h>

namespace Ui {
class Curve;
}

class Curve : public QDialog
{
    Q_OBJECT

public:
    explicit Curve(QWidget *parent = nullptr);
    ~Curve();

    QImage temp;

    double tempRedTable[256];

    double tempGreenTable[256];

    double tempBlueTable[256];

    unsigned int redTable[256];

    unsigned int blueTable[256];

    unsigned int greenTable[256];

    void getPhotoData(QImage ImageData);

    QwtPlotCurve *function;

    QwtPlotPicker *picker;
private slots:

private:
    Ui::Curve *ui;
};

#endif // CURVE_H
