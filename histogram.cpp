#include "histogram.h"
#include "ui_histogram.h"
#include <iostream>

using namespace std;

Histogram::Histogram(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Histogram)
{
    ui->setupUi(this);
}

Histogram::~Histogram()
{
    delete ui;
}
void Histogram::getPhotoData(QImage ImageData){

    temp = ImageData;

    for (int i = 0;i< 256;i++)

        redTable[i] = blueTable[i] = greenTable[i] = 0;

    for (int i = 0;i< temp.width();i++){

        for (int j = 0;j < temp.height();j++){

            QColor clrCurrent( temp.pixel(i,j) );

            redTable[clrCurrent.red()]++;

            blueTable[clrCurrent.blue()]++;

            greenTable[clrCurrent.green()]++;

        }

    }

    redCumulative[0] = blueCumulative[0] = greenCumulative[0] = 0;

    for (int i = 0;i< 256;i++){

        tempRedTable[i] = (double)redTable[i]/((double)temp.height()*(double)temp.width());

        tempBlueTable[i] = (double)blueTable[i]/((double)temp.height()*(double)temp.width());

        tempGreenTable[i] = (double)greenTable[i]/((double)temp.height()*(double)temp.width());

    }

    for (int i = 0;i< 256;i++){

        redCumulative[i] = redCumulative[i-1] + tempRedTable[i]+1E-10;

        blueCumulative[i] = blueCumulative[i-1] + tempBlueTable[i]+1E-10;

        greenCumulative[i] = greenCumulative[i-1] + tempGreenTable[i]+1E-10;
    }

    scene = new QGraphicsScene(this);

    ui->graphicsView->setScene(scene);

    scene->setSceneRect(0, 0,ui->graphicsView->maximumWidth(),ui->graphicsView->maximumHeight());

    ui->graphicsView->scale(1, -1);

    pen.setStyle(Qt::SolidLine);

    pen.setWidth(1);

    for (int i = 0; i < 256; i++){

            redEqual[i] = (uchar)(255 * redCumulative[i] + 0.5);

            blueEqual[i] = (uchar)(255 * blueCumulative[i] + 0.5);

            greenEqual[i] = (uchar)(255 * greenCumulative[i] + 0.5);

    }

}



void Histogram::on_redCheckBox_clicked(){

    pen.setColor(Qt::red);

    if (ui->redCheckBox->isChecked()){

        if(ui->histogramRadioButton->isChecked()){

            for (int i = 0;i<=256;i++)

                line = scene->addLine(i*3,0,i*3,redTable[i],pen);

        }

        if (ui->cumulativeRadioButton->isChecked()){

            for (int i = 0;i<=256;i++)

                line = scene->addLine(i*3,0,i*3,redCumulative[i]*ui->graphicsView->maximumHeight(),pen);

        }


    }

    else {

        for (int i = 0;i<=256;i++)

            line = scene->addLine(i*3,0,i*3,422,QPen(Qt::white));


    }

}



void Histogram::on_greenCheckBox_clicked(){

    pen.setColor(Qt::green);

    if (ui->greenCheckBox->isChecked()){

        if(ui->histogramRadioButton ->isChecked()){

            for (int i = 0;i<=256;i++)

                line = scene->addLine(i*3+1,0,i*3+1,greenTable[i],pen);

        }

        if (ui->cumulativeRadioButton->isChecked()){

            for (int i = 0;i< 256;i++)

                line = scene->addLine(i*3+1,0,i*3+1,greenCumulative[i]*ui->graphicsView->maximumHeight(),pen);

        }


    }

    else{

        for (int i = 0;i<=256;i++)

            line = scene->addLine(i*3+1,0,i*3+1,422,QPen(Qt::white));

    }

}

void Histogram::on_blueCheckBox_clicked(){

    pen.setColor(Qt::blue);

    if(ui->blueCheckBox->isChecked()){

        if(ui->histogramRadioButton ->isChecked()){

            for (int i = 0;i< 256;i++)

                line = scene->addLine(i*3+2,0,i*3+2,blueTable[i],pen);

        }

        if (ui->cumulativeRadioButton->isChecked()){

            for (int i = 0;i< 256;i++)

                line = scene->addLine(i*3+2,0,i*3+2,blueCumulative[i]*ui->graphicsView->maximumHeight(),pen);

        }

    }

    else {

        for (int i = 0;i< 256;i++)

            line = scene->addLine(i*3+2,0,i*3+2,422,QPen(Qt::white));

    }


}

int *Histogram::getRedTable(){

    return redEqual;

}

int *Histogram::getBlueTable(){

    return blueEqual;

}

int *Histogram::getGreenTable(){

    return greenEqual;

}
