#ifndef HISTOGRAM_H
#define HISTOGRAM_H

#include <QDialog>
#include <QImage>
#include <QGraphicsScene>
#include <QGraphicsLineItem>

namespace Ui {
class Histogram;
}

class Histogram : public QDialog
{
    Q_OBJECT

public:
    explicit Histogram(QWidget *parent = nullptr);
    ~Histogram();

    QImage temp;

    unsigned int redTable[256];

    double redCumulative[256];

    double blueCumulative[256];

    double greenCumulative[256];

    unsigned int blueTable[256];

    unsigned int greenTable[256];

    unsigned int MaxHeight;

    void getPhotoData(QImage);

    void drawHistogram();

    int redEqual[266];

    int blueEqual[266];

    int greenEqual[266];

    QPen pen;

    QGraphicsLineItem *line;

    double tempRedTable[256];

    double tempGreenTable[256];

    double tempBlueTable[256];


    int *getRedTable();

    int *getBlueTable();

    int *getGreenTable();

private slots:
    void on_redCheckBox_clicked();

    void on_greenCheckBox_clicked();

    void on_blueCheckBox_clicked();


private:
    Ui::Histogram *ui;

    QGraphicsScene *scene;


};

#endif // HISTOGRAM_H
