#include "level.h"
#include "ui_level.h"

Level::Level(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Level)
{
    ui->setupUi(this);

    scene = new QGraphicsScene (this);

    for (int i = 0;i<256;i++){

        colorTemp.setRgb(i,i,i,255);

        brush .setColor(colorTemp);

        scene->setBackgroundBrush(brush);

    }
}

Level::~Level()
{
    delete ui;
}

void Level::getPhotoData(QImage ImageData){

    temp = ImageData;

    for (int i = 0;i< 256;i++)

        redTable[i] = blueTable[i] = greenTable[i] = 0;

    for (int i = 0;i< temp.width();i++){

        for (int j = 0;j < temp.height();j++){

            QColor clrCurrent( temp.pixel(i,j) );

            redTable[clrCurrent.red()]++;

            blueTable[clrCurrent.blue()]++;

            greenTable[clrCurrent.green()]++;

        }

    }

    for (int i = 0;i< 256;i++){

        tempRedTable[i] = (double)redTable[i]/((double)temp.height()*(double)temp.width());

        tempBlueTable[i] = (double)blueTable[i]/((double)temp.height()*(double)temp.width());

        tempGreenTable[i] = (double)greenTable[i]/((double)temp.height()*(double)temp.width());

    }
}
