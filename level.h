#ifndef LEVEL_H
#define LEVEL_H

#include <QDialog>
#include <QGraphicsScene>
#include <qwt_slider.h>


namespace Ui {
class Level;
}

class Level : public QDialog
{
    Q_OBJECT

public:
    explicit Level(QWidget *parent = nullptr);
    ~Level();


    QGraphicsScene *scene;

    QBrush brush;

    QColor colorTemp;

    QImage temp;

    double tempRedTable[256];

    double tempGreenTable[256];

    double tempBlueTable[256];

    unsigned int redTable[256];

    unsigned int blueTable[256];

    unsigned int greenTable[256];

    void getPhotoData(QImage ImageData);

private:
    Ui::Level *ui;
};

#endif // LEVEL_H
