#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "histogram.h"
#include <iostream>
#include "level.h"
#include "curve.h"

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionOpen_triggered()
{
    fileName = QFileDialog::getOpenFileName(this,tr("Open File"),"/",tr( "Image Files (*.png *.jpg *.bmp)"));

        if (!fileName.isEmpty()){

                QImage image(fileName);

                if (!image.isNull()){

                    temp = image;

                    ui->photoView->setAlignment(Qt::AlignCenter);

                    QGraphicsScene *scene = new QGraphicsScene();

                    scene->addPixmap(QPixmap::fromImage(temp).scaled(ui->photoView->width(),ui->photoView->height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));

                    ui->photoView->setScene(scene);

                    ui->photoView->show();

                }else{

                    errorBox.critical(this,"Error","This is not Photo");

                    errorBox.setFixedSize(500,200);

                }

        }

}


void MainWindow::on_actionHistogram_triggered()
{

    Histogram histogram;

    histogram.setModal(true);

    histogram.getPhotoData(temp);

    QImage reverseTemp = temp;

    redEqual= histogram.getRedTable();

    greenEqual = histogram.getGreenTable();

    blueEqual = histogram.getBlueTable();

    QColor tempColor;

    for (int i = 0;i< temp.width();i++){

        for (int j = 0;j< temp.height();j++){

            QColor rbgTemp = temp.pixel(i,j);

            int redTemp = rbgTemp.red();

            int blueTemp = rbgTemp.blue();

            int greenTemp = rbgTemp.green();

            tempColor.setRed(redEqual[redTemp]);

            tempColor.setBlue(blueEqual[blueTemp]);

            tempColor.setGreen(greenEqual[greenTemp]);

            reverseTemp.setPixelColor(i,j,tempColor);

        }

    }

    ui->showGraphicsView->setAlignment(Qt::AlignCenter);

    QGraphicsScene *scene = new QGraphicsScene();

    scene->addPixmap(QPixmap::fromImage(reverseTemp).scaled(ui->showGraphicsView->width(),ui->showGraphicsView->height(),Qt::KeepAspectRatio,Qt::SmoothTransformation));

    ui->showGraphicsView->setScene(scene);

    ui->showGraphicsView->show();

    histogram.exec();

}



void MainWindow::on_actionLevel_triggered()
{

    Level level;

    level.getPhotoData(temp);

    level.exec();

}

void MainWindow::on_actionCurve_triggered()
{
    Curve curve;

    curve.getPhotoData(temp);

    curve.exec();

}
