#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFileDialog>
#include <QMessageBox>


namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    QString fileName;
    QMessageBox errorBox;

    QImage temp;

    int *redEqual;

    int *greenEqual;

    int *blueEqual;

private slots:
    void on_actionOpen_triggered();

    void on_actionHistogram_triggered();


    void on_actionLevel_triggered();

    void on_actionCurve_triggered();

private:
    Ui::MainWindow *ui;


};

#endif // MAINWINDOW_H
