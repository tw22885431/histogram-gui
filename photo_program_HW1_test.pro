#-------------------------------------------------
#
# Project created by QtCreator 2018-11-21T13:36:22
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = photo_program_HW1_test
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    histogram.cpp \
    level.cpp \
    curve.cpp

HEADERS += \
        mainwindow.h \
    histogram.h \
    level.h \
    curve.h

FORMS += \
        mainwindow.ui \
    histogram.ui \
    histogram.ui \
    level.ui \
    curve.ui
# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../usr/local/qwt-6.1.3/lib/release/ -lqwt
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../usr/local/qwt-6.1.3/lib/debug/ -lqwt
else:unix: LIBS += -L$$PWD/../../../../usr/local/qwt-6.1.3/lib/ -lqwt

INCLUDEPATH += $$PWD/../../../../usr/local/qwt-6.1.3/include
DEPENDPATH += $$PWD/../../../../usr/local/qwt-6.1.3/include


win32:CONFIG(release, debug|release): LIBS += -L$$PWD/../../../../usr/local/qwt-6.1.3/plugins/designer/release/ -lqwt_designer_plugin
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/../../../../usr/local/qwt-6.1.3/plugins/designer/debug/ -lqwt_designer_plugin
else:unix: LIBS += -L$$PWD/../../../../usr/local/qwt-6.1.3/plugins/designer/ -lqwt_designer_plugin

INCLUDEPATH += $$PWD/../../../../usr/local/qwt-6.1.3/plugins/designer
DEPENDPATH += $$PWD/../../../../usr/local/qwt-6.1.3/plugins/designer
